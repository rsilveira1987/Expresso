<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Json
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schüle <p.schuele@metaways.de>
 * @copyright   Copyright (c) 2007-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * Test class for Tinebase_Frontend_Json
 */
class Custom_Tinebase_Frontend_JsonTest extends Tinebase_Frontend_JsonTest
{
    /**
     * Skip this test because Expresso doesn't save advanced search toogle as preference
     */
    public function testAdvancedSearchToogle()
    {
        $this->markTestSkipped('Expresso does not save advanced search toogle as preference');
    }

    /**
     * Skip this test because Expresso doesn't have change user account functionality
     */
    public function testChangeUserAccount()
    {
        $this->markTestSkipped('Expresso does not have change user account functionality');
    }

    /**
     * Expresso has the following locales: de, en, es, pt_BR and locale options count is 5
     * Changed locale option count to granterThan 4
     *
     */
    public function testSearchPreferences()
    {
        // search prefs
        $result = $this->_instance->searchPreferencesForApplication('Tinebase', $this->_getPreferenceFilter());

        // check results
        $this->assertTrue(isset($result['results']));
        $this->assertGreaterThan(2, $result['totalcount']);

        //check locale/timezones options
        foreach ($result['results'] as $pref) {
            switch($pref['name']) {
                case Tinebase_Preference::LOCALE:
                    $this->assertGreaterThan(4, count($pref['options']));
                    break;
                case Tinebase_Preference::TIMEZONE:
                    $this->assertGreaterThan(100, count($pref['options']));
                    break;
            }
            // check label and description
            $this->assertTrue(isset($pref['label']) && !empty($pref['label']));
            $this->assertTrue(isset($pref['description']) && !empty($pref['description']));
        }
    }

    /**
     * Expresso config bool values for false can also be integer (0)
     * maybe due to storing config to file and not DB, so we don't expect
     * false values to be strict FALSE
     */
    public function testGetAllRegistryData()
    {
        $registryData = $this->_instance->getAllRegistryData();
        $currentUser = Tinebase_Core::getUser();

        $this->assertEquals($currentUser->toArray(), $registryData['Tinebase']['currentAccount']);
        $this->assertEquals(
            Addressbook_Controller_Contact::getInstance()->getContactByUserId($currentUser->getId())->toArray(),
            $registryData['Tinebase']['userContact']
        );
        $this->assertEquals(TRUE, $registryData['Tinebase']['config']['changepw']['value'], 'changepw should be TRUE');

        Tinebase_Config::getInstance()->set('changepw', 0);
        $registryData = $this->_instance->getAllRegistryData();
        $changepwValue = $registryData['Tinebase']['config']['changepw']['value'];
        $this->assertEquals(FALSE, $changepwValue, 'changepw should be (bool) false');

        $userApps = $registryData['Tinebase']['userApplications'];
        $this->assertEquals('Admin', $userApps[0]['name'], 'first app should be Admin: ' . print_r($userApps, TRUE));

        $locale = Tinebase_Core::getLocale();
        $symbols = Zend_Locale::getTranslationList('symbols', $locale);
        $this->assertEquals($symbols['decimal'], $registryData['Tinebase']['decimalSeparator']);
    }

    /**
     * User profile is not updatable to Ldap backends,
     * so we don't check if profile data has changed
     */
    public function testUpdateUserProfile()
    {
        $profile = $this->_instance->getUserProfile(Tinebase_Core::getUser()->getId());
        $profileData = $profile['userProfile'];

        $this->assertFalse(array_search('n_prefix', $profileData));

        $profileData['tel_home'] = 'mustnotchange';
        $profileData['email_home'] = 'email@userprofile.set';

        $this->_instance->updateUserProfile($profileData);

        $updatedProfile = $this->_instance->getUserProfile(Tinebase_Core::getUser()->getId());
        $updatedProfileData = $updatedProfile['userProfile'];
        $this->assertNotEquals('mustnotchange', $updatedProfileData['tel_home']);
    }

    /**
     * Expresso don't have the right 'Tinebase_Acl_Rights::USE_PERSONAL_TAGS' by application
     * see Tine2.0 Tinebase_Tags::searchTags() and Tinebase_Setup_Update_Release8::update_10()
     *
     */
    public function testOmitPersonalTagsOnSearch()
    {
        $this->markTestSkipped('Expresso does not have the right Tinebase_Acl_Rights::USE_PERSONAL_TAGS by application');
    }

    /**
     * Use sclever account id instead of hardcoded id=2 (user with id=2 does not exists)
     *
     */
    public function testSearchPreferencesOfOtherUsers()
    {
        $sclever = Tinebase_User::getInstance()->getFullUserByLoginName('sclever');
        // add new default pref
        $pref = $this->_getPreferenceWithOptions();
        $pref->account_id   = $sclever->accountId;
        $pref->account_type = Tinebase_Acl_Rights::ACCOUNT_TYPE_USER;
        $pref = Tinebase_Core::getPreference()->create($pref);

        // search prefs
        $results = $this->_instance->searchPreferencesForApplication('Tinebase', $this->_getPreferenceFilter(TRUE, FALSE, $sclever->accountId));

        // check results
        $this->assertTrue(isset($results['results']));
        $this->assertEquals(1, $results['totalcount']);
    }

}
