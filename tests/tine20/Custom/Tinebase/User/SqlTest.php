<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Account
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

if (!defined('PHPUnit_MAIN_METHOD')) {
    define('PHPUnit_MAIN_METHOD', 'Tinebase_User_SqlTest::main');
}

/**
 * Test class for Tinebase_User
 */
class Custom_Tinebase_User_SqlTest extends Tinebase_User_SqlTest
{
    /**
     * Run User Sql Tests regardless configured backend
     *
     */
    protected function setUp()
    {
        $this->_backend = Tinebase_User::factory(Tinebase_User::SQL);

        // remove user left over by broken tests
        try {
            $user = $this->_backend->getUserByLoginName('tine20phpunituser', 'Tinebase_Model_FullUser');
            $this->_backend->deleteUser($user);
        } catch (Tinebase_Exception_NotFound $tenf) {
            // do nothing
        }

        $this->objects['users'] = array();
    }

}