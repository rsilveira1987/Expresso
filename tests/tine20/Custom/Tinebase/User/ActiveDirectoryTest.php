<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Account
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Jeferson Jose de Miranda <jeferson.miranda@serpro.gov.br>
 */

/**
 * Test class for Tinebase_User
 */
class Custom_Tinebase_User_ActiveDirectoryTest extends Tinebase_User_ActiveDirectoryTest
{
    protected function setUp()
    {
        // do not mark incomplete
    }

    public function testAddUserToSyncBackend()
    {
        $this->markTestSkipped('Expresso does not use Active Directory');
    }

    /**
     * It is necessary to avoid skipping of all tests
     */
    public function testDoNothing()
    {
        $this->assertNull(NULL);
    }
}