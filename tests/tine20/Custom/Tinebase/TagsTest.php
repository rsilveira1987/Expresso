<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 *
 */

/**
 * Test class for Tinebase_TagsTest
 */
class Custom_Tinebase_TagsTest extends Tinebase_TagsTest
{
    /**
     * test if the application parameter is used properly
     */
    public function testSearchTagsForApplication()
    {
        $this->_createSharedTag();
        $filter = new Tinebase_Model_TagFilter(array());
        $ids = $this->_instance->searchTags($filter)->getId();
        $this->_instance->deleteTags($ids);

        $t1 = $this->_createSharedTag('tag1');
        $t2 = $this->_createSharedTag('tag2');

        // this tag should not occur, search is in the addressbook application
        $tasksAppId = Tinebase_Application::getInstance()->getApplicationByName('Tasks')->getId();
        $t3 = $this->_createSharedTag('tag3', array($tasksAppId));

        $filter = new Tinebase_Model_TagFilter(array('application' => 'Addressbook'));

        $tags = $this->_instance->searchTags($filter);
        $this->assertEquals(2, $tags->count());
    }
}
