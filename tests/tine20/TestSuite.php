<?php

/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tests
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

/**
 * Tinebase TestSuite
 *
 * @package     Tests
 */
class TestSuite extends PHPUnit_Framework_TestSuite
{
    /**
     * Default prefix
     */
    const DEFAULT_PREFIX = 'Custom';

    /**
     * Get custom test prefix or false if no custom tests exists
     *
     * @return  mixed bool false | string $prefix
     */
    private function _getTestPrefix()
    {
        $testConfig = Zend_Registry::get('testConfig');

        if (isset($testConfig) && isset($testConfig->customtests)
                && isset($testConfig->customtests->enabled)
                && $testConfig->customtests->enabled === true) {

            $prefix = self::DEFAULT_PREFIX;

            if (isset($testConfig->customtests->prefix)
                    && is_string($testConfig->customtests->prefix)
                    && trim($testConfig->customtests->prefix) !== '') {
                $prefix = trim($testConfig->customtests->prefix);
            }
        } else {
            $prefix = false;
        }

        return $prefix;
    }

    /**
     * Adds the tests from the given class to the suite.
     * If plugin class exists, it is used.
     *
     * @param  mixed $testClass
     */
    public function addTestSuite($testClass)
    {
        $prefix = $this->_getTestPrefix();

        if ($prefix === false) {
            parent::addTestSuite($testClass);
        } else {
            switch ($prefix) {
                case self::DEFAULT_PREFIX:
                    if (@class_exists(self::DEFAULT_PREFIX . '_' . $testClass)) {
                        parent::addTestSuite(self::DEFAULT_PREFIX . '_' . $testClass);
                    } else {
                        parent::addTestSuite($testClass);
                    }
                    break;
                default:
                    if (@class_exists($prefix . '_' . $testClass)) {
                        parent::addTestSuite($prefix . '_' . $testClass);
                    } elseif (@class_exists(self::DEFAULT_PREFIX . '_' . $testClass)) {
                        parent::addTestSuite(self::DEFAULT_PREFIX . '_' . $testClass);
                    } else {
                        parent::addTestSuite($testClass);
                    }
                    break;
            }
        }
    }
}
