<?php
/**
 * Expresso Br
 * Test case that verifies the behavior of the login screen.
 *
 * @package ExpressoBrTest\Functional\Login
 * @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author Rafael Raymundo da Silva <rafael.silva@serpro.gov.br>
 * @author Marcelo Costa Toyama <marcelo.toyama@serpro.gov.br>
 * @copyright Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 */

namespace ExpressoBrTest\Functional\Generic;

class GenericPage
{
    /**
     * @var ExpressoBrTest $testCase The testCase in which the
     * page object is being used
     */
    protected $testCase;

    /**
     * @var unknown $rootContext The root element in which the
     * page object is based. If the page object refers to the whole
     * window, it will be the test case itself. If the page object
     * refers only to a part of the window, $rootContext will be
     * a reference to the root DOM element of that part of the window
     */
    protected $rootContext;

    /**
     * Creates a new GenericPage object. If no root context is informed,
     * it will use the whole test case as root context.
     *
     * @param ExpressoBrTest $testCase The test case to which this
     * page object belongs
     * @param string $rootContext The root context in which operations
     * in the page object are based. @see GenericPage::rootContext for
     * more information
     */
    public function __construct($testCase, $rootContext = null)
    {
        $this->testCase = $testCase;
        $this->rootContext = ($rootContext !== null) ?
            $rootContext :
            $testCase;
        // if no root context is informed, we assume it is
        // the test case main window
    }

    /**
     * Returns the test case to which this page object belongs
     *
     * @return \ExpressoBrTest\Functional\Generic\ExpressoBrTest
     */
    public function getTestCase()
    {
        return $this->testCase;
    }

    /**
     * Returns a single DOM element contained in the root context of
     * this page object based on a CSS selector
     *
     * @param string $cssSelector The CSS selector to be searched
     *
     * @return unknown A reference to a DOM element present in the page
     */
    public function byCssSelector($cssSelector)
    {
        return $this->rootContext->byCssSelector($cssSelector);
    }

    /**
     * Returns an array of multiple DOM elements contained in the
     * root context of this page object based on a CSS selector
     *
     * @param string $cssSelector The CSS selector to be searched
     * @return array An array of references to the DOM elements that
     * match the specified $cssSelector
     */
    public function byCssSelectorMultiple($cssSelector)
    {
        return $this->rootContext->elements($this->rootContext->using('css selector')->value($cssSelector));
    }

    /**
     * Returns the value of an attribute of the DOM element to which this
     * GenericPage instance corresponds
     *
     * @param string The name of the attribute
     * @returns string The value of the attribute
     */
    public function attribute($attrName)
    {
        return $this->rootContext->attribute($attrName);
    }

    /**
     * Types a string in the current browser window, just as if the user
     * was typing on the keyboard.
     *
     * @param string $string The string to be typed
     */
    public function type($string)
    {
        $this->testCase->keys($string);
    }

    /**
     * Checks if a specific element is present within the root context
     * of the page object
     *
     * @param string $cssSelector The css selector to be searched
     * @return boolean True if the element is present, false otherwise
     */
    public function isElementPresent($cssSelector)
    {
        $this->testCase->timeouts()->implicitWait(9000);
        //temporarily decrease wait time so we don't have to wait too long
        //to find out that the element is really not present

        $isPresent =  count($this->byCssSelectorMultiple($cssSelector)) > 0;

        //restore original wait time
        $this->testCase->timeouts()->implicitWait(ExpressoBrTest::DEFAULT_WAIT_INTERVAL);

        return $isPresent;
    }
}
