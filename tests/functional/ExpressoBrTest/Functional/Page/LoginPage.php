<?php
/**
 * Expresso Br
 * Test case that verifies the behavior of the login screen.
 *
 * @package ExpressoBrTest\Functional\Login
 * @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author Rafael Raymundo da Silva <rafael.silva@serpro.gov.br>
 * @author Marcelo Costa Toyama <marcelo.toyama@serpro.gov.br>
 * @copyright Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 */

namespace ExpressoBrTest\Functional\Page;

use ExpressoBrTest\Functional\Generic\GenericPage;

class LoginPage extends GenericPage
{
    /**
     * Types a value in the user field
     *
     * @param string $user The value to be typed into the field
     */
    public function typeUser($user)
    {
        $this->byCssSelector('#username')->clear();
        $this->byCssSelector('#username')->value($user);
    }

    /**
     * Types a value in the password field
     *
     * @param string $password The value to be typed into the field
     */
    public function typePassword($password)
    {
        $this->byCssSelector('#password')->clear();
        $this->byCssSelector('#password')->value($password);
    }

    /**
     * Clicks the login button
     */
    public function clickLogin()
    {
        $this->byCssSelector('#tb-login-panel-button .x-btn-mc')->click();
    }

    /**
     * This will perform a login into the system, involving the following steps:
     * 1 - clear user field, 2- type a user, 3 - type a password,
     * 4 - click loggin button,
     * 5 - Wait for the element (#tine-viewport-maincardpanel) is present after login
     *
     * @param string $user The user to be used for the login
     * @param string $pwd The password to be used for the login
     */
    public function doLogin($user, $pwd)
    {
        $this->typeUser($user);
        $this->typePassword($pwd);
        $this->clickLogin();
        return $this->isElementPresent('#tine-viewport-maincardpanel');
    }

    /**
     * Verify login incorrect
     * @param ext-mb-content 'Incorrect login' present on login screen when you type incorrect user/password
     */
    public function isMbContentPresent(){
        return $this->isElementPresent('.ext-mb-content');
    }
}
