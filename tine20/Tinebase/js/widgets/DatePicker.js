/*
 * Tine 2.0
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2007-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

Ext.ns('Tine.widgets');

/**
 * Customized DatePicker component
 *
 * @namespace   Tine.widgets
 * @class       Tine.widgets.DatePicker
 * @extends     Ext.DatePicker
 */
Tine.widgets.DatePicker = Ext.extend(Ext.DatePicker, {
    /**
     * @cfg {String} todayTip
     * A string used to format the message for displaying in a tooltip over the button that
     * selects the current date. Defaults to <code>'{0}'</code> where
     * the <code>{0}</code> token is replaced by today's date.
     */
    todayTip : '{0}',

    handleDateClick : function(e, t) {
        e.stopEvent();
        if (!this.disabled && t.dateValue && !Ext.fly(t.parentNode).hasClass('x-date-disabled')) {
            this.cancelFocus = this.focusOnSelect === false;
            var date = new Date(t.dateValue);
            if (date.getHours() == 23) { // date should have hour 00:00:00
                // probably its dst first day
                date.setHours(1, 0, 0, 0);
                date.setDate(date.getDate() + 1);
            }
            this.setValue(date);
            delete this.cancelFocus;
            this.fireEvent('select', this, this.value);
        }
    }
});

Ext.reg('widget-datepicker', Tine.widgets.DatePicker);