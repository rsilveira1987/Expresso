<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Server
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 *
 */

/**
 * Allow to inject additional behavior to Tinebase_Server_Http::handle
 *
 * @package     Tinebase
 * @subpackage  Server
 */
interface Tinebase_Server_Plugin_Actions_Interface {

    /**
     * Define inputs for plugins into setup
     */
    public static function init();

    /**
     * Define actions for Tinebase_Server_Http handle
     */
    public function executeAction();
}
