<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Server
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2008-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 */

/**
 * plugin server abstract
 *
 * @package     Tinebase
 * @subpackage  Server
 */
abstract class Tinebase_Server_Plugin_Abstract implements Tinebase_Server_Plugin_Interface
{

    /**
     * @var string
     */
    protected static $_serverClassName;

    /**
     * Sets a custom server class
     * @param string $classname
     * @throws Tinebase_Exception_InvalidArgument
     */
    public static function setServerClass($classname)
    {
        if(get_called_class() == 'Tinebase_Server_Plugin_Abstract') {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: This method cannot be called directly to Tinebase_Server_Plugin_Abstract");
            throw new Tinebase_Exception_AccessDenied('This method cannot be called directly to Tinebase_Server_Plugin_Abstract');
        }

        if(!is_subclass_of($classname, 'Tinebase_Server_Abstract')) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Class provided is not a subclass of Tinebase_Server_Http_Abstract");
            throw new Tinebase_Exception_InvalidArgument("Class provided is not a subclass of Tinebase_Server_Http_Abstract");
        }

        static::$_serverClassName = $classname;
    }

    /**
     * Returns a Tinebase_Server_Abstract object
     * @param string $defaultServerClass
     * @return Tinebase_Server_Abstract
     */
    protected static function _getServer($defaultServerClass)
    {
        if(isset(static::$_serverClassName)) {
            return new static::$_serverClassName();
        }

        return new $defaultServerClass();
    }
}

