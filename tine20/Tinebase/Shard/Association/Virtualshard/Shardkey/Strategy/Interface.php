<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase_Shard
 * @subpackage  Strategy
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * Interface for Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy
 *
 * @package     Tinebase_Shard
 */

Interface Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_Interface
{
    /**
     * Get VirtualShard number associated with shardKey from selected strategy
     *
     * @param  string $_database
     * @param  string $_shardKey
     * @return integer || NULL
     */
    public function getVirtualShard($_database, $_shardKey);
}