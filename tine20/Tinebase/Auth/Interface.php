<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @subpackage  Auth
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2009-2010 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 */

/**
 * authentication backend interface
 *  
 * @package     Tinebase
 * @subpackage  Auth
 */
interface Tinebase_Auth_Interface extends Zend_Auth_Adapter_Interface
{
    /**
     * setIdentity() - set the value to be used as the identity
     *
     * @param  string $value
     * @return Zend_Auth_Adapter_Interface Provides a fluent interface
     */
    public function setIdentity($value);
    
    /**
     * setCredential() - set the credential value to be used
     *
     * @param  string $credential
     * @return Zend_Auth_Adapter_Interface Provides a fluent interface
     */
    public function setCredential($credential);
    
    /**
     * Returns the type of backend
     * @return string
     */
    public static function getType();
        
    /**
     * Returns default configurations of the backend
     * @return array
     */
    public static function getBackendConfigurationDefaults();

    /**
     * Returns a connection to auth backend
     * @param array $_options
     * @return mixed
     * @throws Tinebase_Exception_Backend
     */
    public static function getBackendConnection(array $_options = array());

    /**
     * Checks if user backend is valid
     * @param mixed $_authBackend
     * @return boolean
     */
    public static function isValid($_authBackend);

    /**
     * Force close connection to backend
     */
    public function closeConnection();

    /**
     * Checks if backend must be forced over the configuration
     * [EXPRESSO EXCLUSIVE FEATURE]
     * @return boolean
     */
    public static function isForcedBackend();
}
