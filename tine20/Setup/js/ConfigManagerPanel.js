/*
 * Tine 2.0
 * 
 * @package     Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/*global Ext, Tine*/

Ext.ns('Tine', 'Tine.Setup');

/**
 * Setup Configuration Manager
 * 
 * @namespace   Tine.Setup
 * @class       Tine.Setup.ConfigManagerPanel
 * @extends     Tine.Tinebase.widgets.form.ConfigPanel
 * 
 * <p>Configuration Panel</p>
 * <p><pre>
 * </pre></p>
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * 
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Setup.ConfigManagerPanel
 */
Tine.Setup.ConfigManagerPanel = Ext.extend(Tine.Tinebase.widgets.form.ConfigPanel, {
    
    /**
     * @private
     * panel cfg
     */
    saveMethod: 'Setup.saveConfig',
    registryKey: 'configData',
    defaults: {
        xtype: 'fieldset',
        autoHeight: 'auto',
        defaults: {width: 300},
        defaultType: 'textfield'
    },

    /**
     * customPlugins
     *
     * @property customPlugins
     * @type array
     */
    customPlugins: null,

    /**
     * @private
     * field index counter
     */
    tabIndexCounter: 1,
    
    /**
     * @private
     */
    initComponent: function () {
        this.idPrefix = Ext.id();
        this.keyEscrowPrefix = this.idPrefix + '-keyEscrow-';

        Tine.Setup.ConfigManagerPanel.superclass.initComponent.call(this);
        this.action_saveConfig.setDisabled(false);
    },
    
    /**
     * get tab index for field
     * 
     * @return {Integer}
     */
    getTabIndex: function () {
        return this.tabIndexCounter++;
    },
    
    /**
     * returns config manager form
     * 
     * @private
     * @return {Array} items
     */
    getFormItems: function () {
        // common config for all combos in this setup
        var commonComboConfig = {
            xtype: 'combo',
            listWidth: 300,
            mode: 'local',
            forceSelection: true,
            allowEmpty: false,
            triggerAction: 'all',
            editable: false,
            tabIndex: this.getTabIndex
        };

        this.sqlBackendCombo = new Ext.form.ComboBox(Ext.applyIf({
            name: 'database_adapter',
            fieldLabel: this.app.i18n._('Backend'),
            value: 'pdo_mysql',
            store: [
                ['pdo_mysql', 'MySQL'],
                ['pdo_pgsql', 'PostgreSQL'],
                ['oracle', 'Oracle']
            ],
            listeners: {
                scope: this,
                change: this.onChangeSqlBackend,
                select: this.onChangeSqlBackend
            }
        }, commonComboConfig));

        this.queueBackendCombo = new Ext.form.ComboBox(Ext.applyIf({
            name: 'actionqueue_backend',
            fieldLabel: this.app.i18n._('Backend'),
            value: 'Redis',
            store: [['Redis','Redis']],
            listeners: {
                scope: this,
                change: this.onChangeQueueBackend,
                select: this.onChangeQueueBackend
            }
        }, commonComboConfig));

        this.sqlBackendComboAS = new Ext.form.ComboBox(Ext.applyIf({
            name: 'asdatabase_adapter',
            fieldLabel: this.app.i18n._('Backend'),
            value: 'pdo_mysql',
            store: [
                ['pdo_mysql', 'MySQL'],
                ['pdo_pgsql', 'PostgreSQL'],
                ['oracle', 'Oracle']
            ],
            listeners: {
                scope: this,
                change: this.onChangeSqlBackend,
                select: this.onChangeSqlBackend
            }
        }, commonComboConfig));

        var domainConfig = Tine.Setup.registry.get('domainConfig');
        this.customPlugins = domainConfig[this.domain]['plugins'];
        
        return [{
            title: this.app.i18n._('Database'),
            id: this.idPrefix+'setup-database-group',
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [ 
                this.sqlBackendCombo, 
                {
                    name: 'database_host',
                    fieldLabel: this.app.i18n._('Hostname'),
                    allowBlank: false
                }, {
                    xtype: 'numberfield',
                    name: 'database_port',
                    fieldLabel: this.app.i18n._('Port')
                }, {
                    name: 'database_dbname',
                    fieldLabel: this.app.i18n._('Database'),
                    allowBlank: false
                }, {
                    name: 'database_username',
                    fieldLabel: this.app.i18n._('User'),
                    allowBlank: false
                }, {
                    name: 'database_password',
                    fieldLabel: this.app.i18n._('Password'),
                    inputType: 'password'
                }, {
                    name: 'database_tableprefix',
                    fieldLabel: this.app.i18n._('Prefix')
                }
            ]
        }, {
            // TODO move map panel config to common config panel -> it should not be saved in config.inc.php
            title: this.app.i18n._('Addressbook Map panel'),
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [
                Ext.applyIf({
                    name: 'mapPanel',
                    fieldLabel: this.app.i18n._('Map panel'),
                    value: Tine.Setup.registry.get(this.registryKey)['mapPanel'],
                    store: [[0, this.app.i18n._('disabled')], [1,this.app.i18n._('enabled')]]
                }, commonComboConfig)
            ]
        }, {
            title: this.app.i18n._('Set Separate database for ActiveSync'),
            checkboxToggle: true,
            collapsed: true,
            id: this.idPrefix+'setup-asdatabase-group',
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [
                this.sqlBackendComboAS,
                {
                    name: 'asdatabase_host',
                    fieldLabel: this.app.i18n._('Hostname'),
                    allowBlank: true
                }, {
                    xtype: 'numberfield',
                    name: 'asdatabase_port',
                    fieldLabel: this.app.i18n._('Port')
                }, {
                    name: 'asdatabase_dbname',
                    fieldLabel: this.app.i18n._('Database'),
                    allowBlank: true
                }, {
                    name: 'asdatabase_username',
                    fieldLabel: this.app.i18n._('User'),
                    allowBlank: true
                }, {
                    name: 'asdatabase_password',
                    fieldLabel: this.app.i18n._('Password'),
                    inputType: 'password'
                }, {
                    name: 'asdatabase_tableprefix',
                    fieldLabel: this.app.i18n._('Prefix')
                }
            ]
        }, {
            title: this.app.i18n._('Queue'),
            id: this.idPrefix+'setup-actionqueue-group',
            checkboxToggle: true,
            collapsed: true,
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [this.queueBackendCombo,
            {
                id: this.queueBackendIdPrefix + 'CardLayout',
                xtype: 'panel',
                layout: 'card',
                activeItem: this.queueBackendIdPrefix + 'Redis',
                border: false,
                width: '100%',
                defaults: {border: false},
                items: [{
                    // redis config options
                    id: this.queueBackendIdPrefix + 'Redis',
                    layout: 'form',
                    autoHeight: 'auto',
                    defaults: {
                        width: 300,
                        xtype: 'textfield',
                        tabIndex: this.getTabIndex
                    },
                    items: [{
                        name: 'actionqueue_host',
                        fieldLabel: this.app.i18n._('Hostname'),
                        value: 'localhost'
                    }, {
                        name: 'actionqueue_port',
                        fieldLabel: this.app.i18n._('Port'),
                        xtype: 'numberfield',
                        minValue: 0,
                        value: 6379
                    }]
                }]
            }]
        }, {
            title: this.app.i18n._('Temporary files'),
            id: this.idPrefix+'setup-tmpDir-group',
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'tmpdir',
                fieldLabel: this.app.i18n._('Temporary Files Path'),
                value: this.getRegistryData(this.registryKey).tmpdir
            }]
        }, {
            title: this.app.i18n._('Filestore directory'),
            id: this.idPrefix+'setup-filesDir-group',
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [{
                name: 'filesdir',
                fieldLabel: this.app.i18n._('Filestore Path'),
                value: this.getRegistryData(this.registryKey)['filesdir']
            }]
        }, {
            title: this.app.i18n._('EMAIL'),
            id: this.idPrefix+'setup-email-group',
            collapsed: false,
            items: [{
                name: 'email_maxMessageSize',
                fieldLabel: this.app.i18n._('Maximum allowed message size (bytes)'),
                xtype: 'numberfield',
                value: this.getRegistryData(this.registryKey).maxMessageSize
            }, {
                name: 'email_maxContactAddToUnknown',
                fieldLabel: this.app.i18n._('Unknown Contacts Maximum Import'),
                xtype: 'numberfield',
                value: this.getRegistryData(this.registryKey)['maxContactAddToUnknown'],
                qtip: this.app.i18n._('Maximum contacts to use Unknown Contacts import feature. Above this number the feature will not import any contact. If not set will default to 10'),
                listeners: {
                    render: function(c) {
                        Ext.QuickTips.register({
                            target: c.getEl(),
                            text: c.qtip
                        });
                    }
                }
            }]
        }, {
            title: this.app.i18n._('Redirecting'),
            id: this.idPrefix+'setup-redirecting-group',
            checkboxToggle:true,
            collapsed: true,
            items: [{
                name: 'redirecting_ldapAttribute',
                fieldLabel: this.app.i18n._('Ldap attribute name'),
                value: this.getRegistryData(this.registryKey)['ldapAttribute']
            },{
                name: 'redirecting_cookieName',
                fieldLabel: this.app.i18n._('Cookie name'),
                value: this.getRegistryData(this.registryKey)['cookieName']
            },{
                name: 'redirecting_defaultCookieValue',
                fieldLabel: this.app.i18n._('Default cookie value'),
                value: this.getRegistryData(this.registryKey)['defaultCookieValue']
            }]
        }, {
            title: this.app.i18n._('State Provider'),
            id: this.idPrefix+'setup-stateprovider-group',
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [Ext.applyIf({
                name: 'stateprovider_provider',
                fieldLabel: this.app.i18n._('State Provider'),
                value: 'localStorage',
                store: [['persistent', this.app.i18n._('Persistent')], ['localStorage', this.app.i18n._('LocalStorage')], ['none', this.app.i18n._('None')]]
            }, commonComboConfig)]
        }, {
            title: this.app.i18n._('Digital Certificate'),
            id: this.idPrefix+'setup-certificate-group',
            checkboxToggle: true,
            collapsed: true,
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [
                {
                    name: 'certificate_useKeyEscrow',
                    xtype: 'checkbox',
                    fieldLabel: this.app.i18n._('Use Master Digital Certificate for Message Encryption'),
                    tabIndex: this.getTabIndex,
                    value: this.getRegistryData(this.registryKey)['useKeyEscrow'],
                    listeners: {
                        scope: this,
                        check: this.onChangeUseKeyEscrowCheckbox
                    }
                },
                {
                    name: 'certificate_masterCertificate',
                    fieldLabel: this.app.i18n._('Master Digital Certificate Path'),
                    value: this.getRegistryData(this.registryKey)['masterCertificate'],
                    xtype: 'textfield',
                    tabIndex: this.getTabIndex,
                    hidden: true
                }
            ]
        }, {
            title: this.app.i18n._('Plugins'),
            id: this.idPrefix+'setup-plugins-group',
            checkboxToggle: true,
            collapsed: true,
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: this.customPlugins
        }, {
            title: this.app.i18n._('Browser Compatibility'),
            id: this.idPrefix+'setup-compatibility-group',
            checkboxToggle: true,
            collapsed: true,
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [
                {
                    name: 'compatibility_browserCompatibilityEnabled',
                    fieldLabel: this.app.i18n._('Enable browser compatibility warning'),
                    xtype: 'checkbox',
                    value: this.getRegistryData(this.registryKey)['browserCompatibilityEnabled']
                },
                {
                    name: 'compatibility_browserCompatibilityInfoPath',
                    fieldLabel: this.app.i18n._('Path to "more information"'),
                    value: this.getRegistryData(this.registryKey)['browserCompatibilityInfoPath']
                }
            ]
        }, {
            title: this.app.i18n._('Access Log'),
            id: this.idPrefix+'setup-accesslog-group',
            defaults: {
                width: 300,
                tabIndex: this.getTabIndex
            },
            items: [
                Ext.applyIf({
                    name: 'accesslog_disableactivesyncaccesslog',
                    fieldLabel: this.app.i18n._('Disable ActiveSync Access Log'),
                    value: this.getRegistryData(this.registryKey)['disableactivesyncaccesslog'],
                    store: [[false, this.app.i18n._('No')], [true,this.app.i18n._('Yes')]]
                }, commonComboConfig),
                Ext.applyIf({
                    name: 'accesslog_disabledavaccesslog',
                    fieldLabel: this.app.i18n._('Disable DAV Access Log'),
                    value: this.getRegistryData(this.registryKey)['disabledavaccesslog'],
                    store: [[false, this.app.i18n._('No')], [true,this.app.i18n._('Yes')]]
                }, commonComboConfig),
            ]
        }];
    },

    /**
     * Change default ports when database adapter gets changed
     */
    onChangeSqlBackend: function () {
        // @todo change default port
    },

    /**
     * Change queue card layout depending on selected combo box entry
     */
    onChangeQueueBackend: function () {
        this.changeCard(this.queueBackendCombo, this.queueBackendIdPrefix);
    },

    /**
     *
     */
    onChangeUseKeyEscrowCheckbox: function(checkbox) {
        var field = checkbox.ownerCt.findBy(function (item){
            if (item.name == 'certificate_masterCertificate'){
                return true;
            }
            return false;
        });
        Ext.each(field, function(item){
            item.setVisible(checkbox.getValue());
        });
    },

    /**
     * Change cache card layout depending on selected combo box entry
     */
    onChangeCacheBackend: function () {
        this.changeCard(this.cacheBackendCombo, this.cacheBackendIdPrefix);
    },

    /**
     * @private
     */
    onRender: function (ct, position) {
        Tine.Setup.ConfigManagerPanel.superclass.onRender.call(this, ct, position);
    },

    /**
     * update setup registry
     */
    afterSaveConfig: function(regData) {
        if (!Tine.Tinebase.registry.get('multidomain') || this.domain == Tine.Setup.registry.get('domainData')['activeDomain']) {
            // replace some registry data
            for (key in regData) {
                if (key != 'status') {
                    Tine.Setup.registry.replace(key, regData[key]);
                }
            }
        }
    },

    /**
     * applies registry state to this cmp
     */
    applyRegistryState: function () {
        if (!Tine.Tinebase.registry.get('multidomain') || this.domain == Tine.Setup.registry.get('domainData')['activeDomain']) {
            Ext.getCmp(this.idPrefix+'setup-actionqueue-group').setIconClass(Tine.Setup.registry.get('checkQueue') ? 'setup_checks_success' : 'setup_checks_fail');
            Ext.getCmp(this.idPrefix+'setup-tmpDir-group').setIconClass(Tine.Setup.registry.get('checkTmpDir') ? 'setup_checks_success' : 'setup_checks_fail');
            Ext.getCmp(this.idPrefix+'setup-filesDir-group').setIconClass(Tine.Setup.registry.get('checkFilesDir') ? 'setup_checks_success' : 'setup_checks_fail');
        }
    },
    
    /**
     * @private
     */
    initActions: function () {
        this.action_downloadConfig = new Ext.Action({
            text: this.app.i18n._('Download config file'),
            iconCls: 'setup_action_download_config',
            scope: this,
            handler: this.onDownloadConfig
        });
        
        this.actionToolbarItems = [this.action_downloadConfig];
        
        Tine.Setup.ConfigManagerPanel.superclass.initActions.apply(this, arguments);
    },
    
    onDownloadConfig: function() {
        if (this.isValid()) {
            var configData = this.form2config();
            
            var downloader = new Ext.ux.file.Download({
                url: Tine.Tinebase.tineInit.requestUrl,
                params: {
                    method: 'Setup.downloadConfig',
                    data: Ext.encode(configData)
                }
            });
            downloader.start();
        } else {
            this.alertInvalidData();
        }
    }
});
