<?php
/**
 * Tine 2.0
 *
 * @package     Calendar
 * @subpackage  Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @copyright   Copyright (c) 2013 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schüle <p.schuele@metaways.de>
 */
class Calendar_Setup_Update_Release7 extends Setup_Update_Abstract
{
    /**
     * update to 7.1
     * - add default grant for anyone to resources
     */
    public function update_0()
    {
        Calendar_Controller_Resource::getInstance()->doContainerACLChecks(FALSE);
        $resources = Calendar_Controller_Resource::getInstance()->getAll();
        foreach ($resources as $resource) {
            $grants = Tinebase_Container::getInstance()->getGrantsOfContainer($resource->container_id, TRUE);
            if (count($grants) === 0) {
                $grants = new Tinebase_Record_RecordSet('Tinebase_Model_Grants', array(array(
                    'account_type' => 'anyone',
                    'account_id' => 0,
                    'readGrant' => TRUE
                )));
                $result = Tinebase_Container::getInstance()->setGrants($resource->container_id, $grants, TRUE, FALSE);
                
                if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
                    . ' Added anyone grant (READ) for resource ' . $resource->name);
            }
        }
        
        $this->setApplicationVersion('Calendar', '7.1');
    }
    
    /**
     * update to 6.2
     * 
     * @see 0008196: Preferences values contains translated value
     */
    public function update_1()
    {
        $release6 = new Calendar_Setup_Update_Release6($this->_backend);
        $release6->update_1();
        $this->setApplicationVersion('Calendar', '7.2');
    }
    
    /**
     * add modlog columns to cal_attendee
     * 
     * @see 0008078: concurrent attendee change should be merged
     */
    public function update_2()
    {
        $modlogCols = array(
                '<field>
                    <name>created_by</name>
                    <type>text</type>
                    <length>40</length>
                </field>',
                '<field>
                    <name>creation_time</name>
                    <type>datetime</type>
                </field>',
                '<field>
                    <name>last_modified_by</name>
                    <type>text</type>
                    <length>40</length>
                </field>',
                '<field>
                    <name>last_modified_time</name>
                    <type>datetime</type>
                </field>',
                '<field>
                    <name>is_deleted</name>
                    <type>boolean</type>
                    <default>false</default>
                </field>',
                '<field>
                    <name>deleted_by</name>
                    <type>text</type>
                    <length>40</length>
                </field>',
                '<field>
                    <name>deleted_time</name>
                    <type>datetime</type>
                </field>',
                '<field>
                    <name>seq</name>
                    <type>integer</type>
                    <notnull>true</notnull>
                    <default>0</default>
                </field>'
        );
        foreach ($modlogCols as $col) {
            $declaration = new Setup_Backend_Schema_Field_Xml($col);
            $this->_backend->addCol('cal_attendee', $declaration);
        }
        $this->setTableVersion('cal_attendee', 4);
        $this->setApplicationVersion('Calendar', '7.3');
    }
    
    /**
     * update to 7.4
     * - make name and creator an unique index
     * 
     */
    public function update_3()
    {
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>cal_event_id</name>
                <field>
                    <name>cal_event_id</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('cal_attendee', $declaration);
        
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>displaycontainer_id</name>
                <field>
                    <name>displaycontainer_id</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('cal_attendee', $declaration);
        
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>creation_time</name>
                <field>
                    <name>creation_time</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('cal_events', $declaration);
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>deleted_time</name>
                <field>
                    <name>deleted_time</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('cal_events', $declaration);
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>last_modified_time</name>
                <field>
                    <name>last_modified_time</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('cal_events', $declaration);
        
        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>cal_event_id</name>
                <field>
                    <name>cal_event_id</name>
                </field>
            </index>
        ');
        $this->_backend->addIndex('cal_exdate', $declaration);
        
        $this->setTableVersion('cal_attendee', 5);
        $this->setTableVersion('cal_events', 7);
        $this->setTableVersion('cal_exdate', 2);
        $this->setApplicationVersion('Calendar', '7.4');
    }

    /**
     * update to 7.5
     * - adds etag column
     * - adds external_seq column
     *
     * @see 0009890: improve external event invitation support
     */
    public function update_4()
    {
        $declaration = new Setup_Backend_Schema_Field_Xml('
            <field>
                <name>etag</name>
                <type>text</type>
                <length>60</length>
            </field>');
        $this->_backend->addCol('cal_events', $declaration);

        $declaration = new Setup_Backend_Schema_Index_Xml('
            <index>
                <name>etag</name>
                <field>
                    <name>etag</name>
                </field>
            </index>');
        $this->_backend->addIndex('cal_events', $declaration);

        if (! $this->_backend->columnExists('external_seq', 'cal_events')) {
            $seqCol = '<field>
                <name>external_seq</name>
                <type>integer</type>
                <notnull>true</notnull>
                <default>0</default>
            </field>';

            $declaration = new Setup_Backend_Schema_Field_Xml($seqCol);
            $this->_backend->addCol('cal_events', $declaration);
        }

        $this->setTableVersion('cal_events', 8);
        $this->setApplicationVersion('Calendar', '7.5');
    }

    /**
     * update to 7.6
     * - move external organizer events to new container
     *
     */
    public function update_5()
    {
        $eventBE = new Tinebase_Backend_Sql(array(
                'modelName'    => 'Calendar_Model_Event',
                'tableName'    => 'cal_events',
                'modlogActive' => false
        ));

        $config = Tinebase_Config::getInstance()->get('externalEventId');

        if ($config && $config['externalEventId'] && $config['externalEventId'] != 0) {
            try {
                $containerId = (int) $config['externalEventId'];
                $externalContainerId = Tinebase_Container::getInstance()->getContainerById($containerId)->id;

                // find all events from old external container
                $eventIds = $this->_db->query(
                        "SELECT " . $this->_db->quoteIdentifier('id') .
                        " FROM " . $this->_db->quoteIdentifier(SQL_TABLE_PREFIX . "cal_events") .
                        " WHERE " . $this->_db->quoteInto($this->_db->quoteIdentifier("container_id") . ' = ?', $externalContainerId)
                )->fetchAll(Zend_Db::FETCH_ASSOC);

                foreach ($eventIds as $eventId) {
                    $event = $eventBE->get($eventId['id']);

                    if (!empty($event->organizer)) {
                        $organizer = $event->resolveOrganizer();
                        if ($organizer instanceof Addressbook_Model_Contact) {
                            $newContainer = Calendar_Controller::getInstance()->getInvitationContainer($organizer);

                            $where  = array(
                                $this->_db->quoteInto($this->_db->quoteIdentifier('id') . ' = ?', $eventId),
                            );

                            try {
                                $this->_db->update(SQL_TABLE_PREFIX . "cal_events", array('container_id' => $newContainer->getId()), $where);
                            } catch (Tinebase_Exception_Record_Validation $terv) {
                                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                                    . ' Could not fix invalid event record: ' . print_r($event->toArray(), true));
                                Tinebase_Exception::log($terv);
                            }
                        }
                    }
                }
            } catch (Tinebase_Exception_NotFound $tenf) {
                Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' External events container not found. Nothing to do.' );
            }
        } else {
            Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' External events container not found. Nothing to do.' );
        }

        $this->setApplicationVersion('Calendar', '7.6');
    }

    /**
     * - update import / export
     */
    public function update_6()
    {
        Setup_Controller::getInstance()->createImportExportDefinitions(Tinebase_Application::getInstance()->getApplicationByName('Calendar'));
        $this->setApplicationVersion('Calendar', '7.7');
    }

    /**
     * update to 8.0
     *
     * @return void
     */
    public function update_7()
    {
        $this->setApplicationVersion('Calendar', '8.0');
    }
}