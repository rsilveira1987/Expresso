<?php
/**
 * Backend Factory for Webconference
 * @package     Webconference
 * @subpackage  Backend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 */
class Webconference_Backend
{
    const BIGBLUEBUTTONAPI = 'BigBlueButtonApi';

    public static function factory($backendType)
    {
        switch ($backendType){
            case self::BIGBLUEBUTTONAPI:
                $default = 'Webconference_Backend_BigBlueButtonApi';
                $backend = Webconference_Config::getInstance()->get(self::BIGBLUEBUTTONAPI, NULL);
                if ($backend == NULL){
                    $backend = $default;
                }
                break;
            default:
                $backend = NULL;
        }
        return new $backend();
    }
}