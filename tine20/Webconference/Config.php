<?php
/**
 * @package     Webconference
 * @subpackage  Config
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2011 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * webconference config class
 * 
 * @package     Webconference
 * @subpackage  Config
 */
class Webconference_Config extends Tinebase_Config_Abstract
{
    /**
     * Attendee Roles Available
     * 
     * @var string
     */
    const ATTENDEE_ROLES = 'wconfRoles';
    const ROOM_STATUS = 'roomStatus';
    /**
     * Maximum number of rooms each month
     *
     * @var string
     */
    const LIMIT_ROOM_MONTH = 'limitRoomMonth';
    
    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Definition::$_properties
     */
    protected static $_properties = array(
        self::ATTENDEE_ROLES => array(
            'label'                 => 'Attendee Roles Available',
            'description'           => 'Possible room attendee roles. Please note that additional attendee roles might impact other webconference systems on export or syncronisation.',
            'type'                  => 'keyFieldConfig',
            'options'               => array('recordModel' => 'Webconference_Model_AttendeeRole'),
            'clientRegistryInclude' => TRUE,
            'default'               => 'MODERATOR'
        ),

        self::ROOM_STATUS => array(
            'label'                 => 'Room Status Available',
            'description'           => 'Possible room status.',
            'type'                  => 'keyFieldConfig',
            'options'               => array('recordModel' => 'Webconference_Model_RoomStatus'),
            'clientRegistryInclude' => TRUE,
            'default'               => 'A'
        ),
        self::LIMIT_ROOM_MONTH => array(
            'label'                 => 'Limit Room Month',
            'description'           => 'Maximum amount of rooms in a month.',
            'type'                  => self::TYPE_INT,
            'clientRegistryInclude' => FALSE,
            'default'               => 0
        )
    );
    
    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Abstract::$_appName
     */
    protected $_appName = 'Webconference';
    
    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_Config
     */
    private static $_instance = NULL;
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */    
    private function __construct() {}
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */    
    private function __clone() {}
    
    /**
     * Returns instance of Tinebase_Config
     *
     * @return Tinebase_Config
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }
    
    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Abstract::getProperties()
     */
    public static function getProperties()
    {
        return self::$_properties;
    }
}
