/**
 * OperationType Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.enumeration;

/**
 *
 * @author Mário César Kolling <mario.kolling@serpro.gov.br>
 */
public enum OperationType {
    SIGN, VERIFY, ENCRYPT, DECRYPT, SIGN_AND_ENCRYPT;
}
