/**
 * WrongPasswordException Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.exception;

/**
 *
 * @author Mário César Kolling <mario.kolling@serpro.gov.br>
 * @todo i18n
 */
public class WrongPasswordException extends Exception {

    public WrongPasswordException(String string) {
        super(string);
    }

    public WrongPasswordException() {
        this("Senha Incorreta");
    }
}
