/**
 * ExpressoCertificateException Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Humberto
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.exception;

/**
 *
 * @author Humberto
 */
public class ExpressoCertificateException extends Exception {

    public ExpressoCertificateException() {
    }

    public ExpressoCertificateException(String message) {
        super(message);
    }

    public ExpressoCertificateException(String message, Throwable cause) {
        super(message, cause);
    }
}
