/**
 * DialogCancelledException Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.exception;

/**
 *
 * @author Mario César Kolling <mario.kolling@serpro.gov.br>
 */
public class DialogCancelledException extends Exception {
    
    public DialogCancelledException(String string) {
        super(string);
    }

    public DialogCancelledException() {
        this("Dialog cancelled");
    }
    
}
