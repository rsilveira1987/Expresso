/**
 * NSSConfigBuilder Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.logging.Logger;
import br.gov.serpro.expresso.security.applet.openmessage.NSSConfig.DbMode;
import br.gov.serpro.expresso.security.applet.openmessage.NSSConfig.Mechanism;
import br.gov.serpro.expresso.security.applet.openmessage.NSSConfig.Module;

public class NSSConfigBuilder {

    private static final Logger logger = Logger.getLogger(NSSConfigBuilder.class.getName());

    private NSSConfigBuilder() {}

    public static NSSConfigBuilder create() {
        return new NSSConfigBuilder();
    }

    final private Deque<File> nssLibraryDirectoryScanPaths = new ArrayDeque<File>();
    {
        nssLibraryDirectoryScanPaths.add(new File("C:/Program Files/Mozilla Firefox"));
        nssLibraryDirectoryScanPaths.add(new File("/opt/Mozilla Firefox"));
    }

    final private Deque<File> nssSecmodDirectoryScanPaths = new ArrayDeque<File>();
    {
        //TODO
        nssSecmodDirectoryScanPaths.add(new File("C:/Windows/System32"));
        nssSecmodDirectoryScanPaths.add(new File("/usr/lib"));
    }

    private String name;
    private String description;
    private DbMode nssDbMode;
    private Module nssModule;
    final private List<Mechanism> enabledMechanisms = new ArrayList<Mechanism>();
    final private List<Mechanism> disabledMechanisms = new ArrayList<Mechanism>();
    private Boolean showInfo;
    //TODO: attributes

    public NSSConfigBuilder nssLibraryDirectoryScanPath(String path) {
        this.nssLibraryDirectoryScanPaths.addFirst(new File(path));
        return this;
    }

    public NSSConfigBuilder nssSecmodDirectoryScanPath(String path) {
        this.nssSecmodDirectoryScanPaths.addFirst(new File(path));
        return this;
    }

    public NSSConfigBuilder name(String name) {
        this.name = name;
        return this;
    }

    public NSSConfigBuilder description(String description) {
        this.description = description;
        return this;
    }

    public NSSConfigBuilder nssDbMode(DbMode nssDbMode) {
        this.nssDbMode = nssDbMode;
        return this;
    }

    public NSSConfigBuilder nssModule(Module nssModule) {
        this.nssModule = nssModule;
        return this;
    }

    public NSSConfigBuilder enableMechanism(Mechanism... mechanism) {
        for(Mechanism m : mechanism) {
            if(enabledMechanisms.contains(m)) continue;
            enabledMechanisms.add(m);
        }
        return this;
    }

    public NSSConfigBuilder disableMechanism(Mechanism... mechanism) {
        for(Mechanism m : mechanism) {
            if(disabledMechanisms.contains(m)) continue;
            disabledMechanisms.add(m);
        }
        return this;
    }

    public NSSConfigBuilder showInfo(boolean showInfo) {
        this.showInfo = showInfo;
        return this;
    }

    public NSSConfig build() {

        String nssLibraryDirectory = null;
        String nssSecmodDirectory = null;

        for(File scanPath : nssLibraryDirectoryScanPaths) {
            File nssWindowsPath = new File(scanPath, "nss3.dll");
            File nssLinuxPath = new File(scanPath, "libnss3.so");
            if(nssWindowsPath.canRead() || nssLinuxPath.canRead()) {
                nssLibraryDirectory = scanPath.toString();
                break;
            }
        }

        for(File scanPath : nssSecmodDirectoryScanPaths) {
//            Path secmodDbPath = scanPath.resolve("secmod.db");
//            Path key3DbPath = scanPath.resolve("key3.db");
            File cert8DbPath = new File(scanPath, "cert8.db");
            if(cert8DbPath.canRead()) {
                nssSecmodDirectory = scanPath.toString();
                break;
            }
        }

        return new NSSConfig(
            name,
            description,
            nssLibraryDirectory,
            nssSecmodDirectory,
            nssDbMode,
            nssModule,
            enabledMechanisms,
            disabledMechanisms,
            showInfo);
    }
}
