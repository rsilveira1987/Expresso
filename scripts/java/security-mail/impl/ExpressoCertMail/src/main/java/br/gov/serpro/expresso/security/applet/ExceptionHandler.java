/**
 * ExceptionHandler Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */
package br.gov.serpro.expresso.security.applet;

public interface ExceptionHandler {
    void onException(Exception ex);
}
