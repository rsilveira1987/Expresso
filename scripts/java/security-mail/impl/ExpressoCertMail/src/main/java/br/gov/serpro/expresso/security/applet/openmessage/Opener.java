/**
 * Opener Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimePart;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.message.DefaultMessageBuilder;
import org.apache.james.mime4j.stream.MimeConfig;
import br.gov.serpro.expresso.security.applet.SmimeApplet;
import br.gov.serpro.expresso.security.cert.DigitalCertificate;
import br.gov.serpro.expresso.security.exception.ExpressoCertificateException;
import static java.util.logging.Level.*;
import static br.gov.serpro.expresso.security.applet.Utils.*;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.*;

public class Opener {

    private static final Logger logger = Logger.getLogger(Opener.class.getName());

    public static final Charset Windows_1252 = Charset.forName("windows-1252");
    public static final Charset UTF8 = Charset.forName("utf-8");

    private static final String CRLF = "\015\012";

    private DigitalCertificate dc;
    private final Decrypter decrypter;
    private final SignatureChecker signatureChecker;
    private final Converter converter;

    private final DefaultMessageBuilder messageBuilder;

    public Opener(Decrypter decrypter, SignatureChecker signatureChecker, Converter converter) {
        this.decrypter = decrypter;
        this.signatureChecker = signatureChecker;
        this.converter = converter;

        messageBuilder = new DefaultMessageBuilder();
        messageBuilder.setContentDecoding(true);

        MimeConfig config = new MimeConfig();
        config.setStrictParsing(false);
        config.setMaxLineLen(-1);
        config.setMaxContentLen(-1);
        config.setMaxHeaderLen(-1);
        config.setMaxHeaderCount(-1);

        messageBuilder.setMimeEntityConfig(config);
    }

    static class Buffer extends ByteArrayOutputStream {
        public byte[] getBuf() {
            return buf;
        }
        public Buffer() {
            super(4096);
        }
    }

    public void setDigitalcertificate(DigitalCertificate dc) {
        this.dc = dc;
    }

    public ExpressoMessage process(String eml) {
        return process(eml.getBytes(UTF8));
    }

    public ExpressoMessage process(byte[] eml) {
        return process(new ByteArrayInputStream(eml));
    }

    public ExpressoMessage process(InputStream eml) {

        MimePart msg1 = null; //JavaMail
        Message msg2 = null;  //Mime4J

        final ExpressoMessage exprMsg = new ExpressoMessage();
        exprMsg.setHasAttachment(false);
        exprMsg.setHasSignature(false);

        Buffer header = null;

        Properties props = System.getProperties();
        Session session = Session.getDefaultInstance(props, null);

        try {
            msg1 = new MimeMessage(session, eml);
        }
        catch (MessagingException ex) {
            raise(ex);
        }

        //assumindo a prática mais comum de que a mensagem foi
        //assinada antes e então encriptada

        // defensive test
        if(isSMIMEEncrypted(msg1)) {

            StringBuilder sb = new StringBuilder();
            Enumeration<String> headerLines = null;

            try {
                headerLines = msg1.getAllHeaderLines();
            }
            catch (MessagingException ex) {
                raise(ex);
            }

            while(headerLines.hasMoreElements()) {
                String line = headerLines.nextElement();
                line = line.replaceFirst("^(?i)(\\s)*Content-Type(\\s)*:", "Encrypted-Content-Type:");
                line = line.replaceFirst("^(?i)(\\s)*Content-Transfer-Encoding(\\s)*:", "Encrypted-Content-Transfer-Encoding:");
                line = line.replaceFirst("^(?i)(\\s)*Content-Disposition(\\s)*:", "Encrypted-Content-Disposition:");
                line = line.replaceFirst("^(?i)(\\s)*Content-Description(\\s)*:", "Encrypted-Content-Description:");
                sb.append(line).append(CRLF);
            }

            header = new Buffer();
            try {
                header.write(sb.toString().getBytes("US-ASCII"));
            }
            catch (IOException ex) {
                raise(ex);
            }

            if (dc == null) {
                msg1 = decrypter.process(msg1);
            } else { // When using Expresso's original Cryptography Library
                try {
                    dc.init();
                    msg1 = dc.readEncryptedMail(msg1);
                    if (msg1 == null) {
                        raise(CryptoException.Error.OPERATION_CANCELLED);
                    }
                } catch (ExpressoCertificateException ex) {
                    // TODO: Get events and raise the correct erros
                    raise(ex);
                }
            }

            //TODO: remover antes da distribuição. Somente para desenvolvimento
//            try {
//                msg1.writeTo(new FileOutputStream(new File(System.getProperty("user.home"), System.currentTimeMillis() + ".eml")));
//            }
//            catch (Exception ex) {
//                raise(ex);
//            }

        }

        if(isSMIMESigned(msg1)) {
            signatureChecker.process(msg1, exprMsg);
        }

        try {

            Buffer buffer = new Buffer();
            msg1.writeTo(buffer);
            
            if(header != null) {

                exprMsg.setSize(header.size() + buffer.size());

                msg2 = messageBuilder.parseMessage(new SequenceInputStream(
                        new ByteArrayInputStream(header.getBuf(), 0, header.size()),
                        new ByteArrayInputStream(buffer.getBuf(), 0, buffer.size())));
            }
            else {

                exprMsg.setSize(buffer.size());

                msg2 = messageBuilder.parseMessage(
                        new ByteArrayInputStream(buffer.getBuf(), 0, buffer.size()));
            }

            converter.process(msg2, exprMsg);
        }
        catch (MessagingException ex) {
            raise(ex);
        }
        catch (IOException ex) {
            raise(ex);
        }

        return exprMsg;
    }

    private boolean isSMIMEEncrypted(MimePart mimePart) {
        try {
            return (mimePart.isMimeType("application/x-pkcs7-mime") || mimePart.isMimeType("application/pkcs7-mime"))
                    && (mimePart.getContentType().contains("enveloped-data"));
        }
        catch (MessagingException ex) {
            raise(ex);
        }
        return false;
    }

    private boolean isSMIMESigned(MimePart mimePart) {
        try {
            return mimePart.isMimeType("multipart/signed")
             // || mimePart.isMimeType("application/x-pkcs7-signature") || mimePart.isMimeType("application/pkcs7-signature")
                || ((mimePart.isMimeType("application/x-pkcs7-mime") || mimePart.isMimeType("application/pkcs7-mime"))
                     && mimePart.getContentType().contains("signed-data"));
        }
        catch (MessagingException ex) {
            raise(ex);
        }
        return false;
    }

    //==========================================================================
    public static void main(String[] args) {

        try {

            SmimeApplet applet = new SmimeApplet() {

                @Override
                public void init() {

                    try {
                        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    }
                    catch (Exception e) {
                        logger.log(WARNING, e.getMessage(), e);
                        e.printStackTrace();
                    }

                    webServer = pico.getComponent(WebServer.class);
                    opener = pico.getComponent(Opener.class);
                }
            };

            applet.init();
            applet.start();

            for(;;) {

                File eml = emlFile();
                String message = com.google.common.io.Files.toString(eml, Windows_1252);
                ExpressoMessage exprMsg = applet.openMessage(message);

                asJsonString(exprMsg);

                int i = System.in.read();
                if (i != 10) { //LF
                    break;
                }
            }

            applet.stop();
            applet.destroy();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static File emlFile() {
        final JFileChooser msgChooser = new JFileChooser();
        FileFilter emlFilter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory() || pathname.getAbsolutePath().endsWith(".eml");
            }
            @Override
            public String getDescription() {
                return "Email messages";
            }
        };
        msgChooser.setFileFilter(emlFilter);

        int msgRet = msgChooser.showOpenDialog(null);

        File eml = null;
        if (msgRet == JFileChooser.APPROVE_OPTION) {
            eml = msgChooser.getSelectedFile();
        }
        else {
            System.out.println("Escolha um arquivo");
            System.exit(1);
        }

        return eml;
    }
}
