/**
 * ExpressoAttachment Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

/*
 * [
 { name: 'id' },
 { name: 'account_id' },
 { name: 'subject' },
 { name: 'from_email' },
 { name: 'from_name' },
 { name: 'sender' },
 { name: 'to' },
 { name: 'cc' },
 { name: 'bcc' },
 { name: 'sent',     type: 'date', dateFormat: Date.patterns.ISO8601Long },
 { name: 'received', type: 'date', dateFormat: Date.patterns.ISO8601Long },
 { name: 'flags' },
 { name: 'size' },
 { name: 'body',     defaultValue: undefined },
 { name: 'headers' },
 { name: 'content_type' },
 { name: 'body_content_type' },
 { name: 'structure' },
 { name: 'attachments' },
 { name: 'original_id' },
 { name: 'folder_id' },
 { name: 'note' },
 { name: 'preparedParts' }, // contains invitation event record
 { name: 'reading_conf' },
 { name: 'smime' },
 { name: 'signature_info' },
 { name: 'importance' },
 { name: 'embedded_images' } 
 ]
 */
package br.gov.serpro.expresso.security.model;

/**
 *
 */
public class Message {
}
