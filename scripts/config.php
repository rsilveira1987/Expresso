#!/usr/bin/env php
<?php
/**
 * Tine 2.0 config change script
 * - This script allow to change config files
 *
 * @package     HelperScripts
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 * @version     $Id$
 */
if (php_sapi_name() != 'cli') {
    die('Not allowed: wrong sapi name!');
}

set_include_path(__DIR__.'/../tine20/');
require 'bootstrap.php';

try {
    $command = (isset($argv[1]))? $argv[1]: '';
    $name = (isset($argv[2]))? $argv[2]: '';
    $returnType = false;
    switch ($command)
    {
        case 'getValue':
            $returnType = true;
        case 'get':
            if ($name === ''){
                showConfigHelp($argv[0]);
                exit;
            }
            $domain = isset($argv[3]) ? $argv[3] : NULL;
            if (!empty($domain)){
                Tinebase_Config::setDomain($domain);
            }
            $json = (array) json_decode($name);
            if (count($json) > 0){
                $value = Tinebase_Config::getInstance()->get(key($json))->{current($json)};
            }else {
                $value = Tinebase_Config::getInstance()->get($name);
            }
            $value = print_r($value, true);    
            if (empty($value)) {
                echo "\nThere is no value for $name" . (empty($domain) ? ' into global file' : ' into domain file') . ".\n\n";
            }else {
                if (!$returnType) {
                    $value = "\nValue of $name is " . $value . "\n";
                }
                echo $value."\n";
            }            
            break;
        case 'set':
            if (!(isset($argv[3]))){
                showConfigHelp($argv[0]);
                exit;
            }
            $value = $argv[3];
            $domain = isset($argv[4]) ? $argv[4] : NULL;
            if (!empty($domain)){
                Tinebase_Config::setDomain($domain);
            }
            $json = (array) json_decode($value);
            if (!empty($json)){
                $value = $json;
                echo "\nValue to be set is: " . print_r($value, true) . "\n";
            }
            $currentValue = Tinebase_Config::getInstance()->get($name);
            if (is_object($currentValue)){
                $currentValue = $currentValue->toArray();
                $value = array_merge($currentValue, $value);
            }
            $file = (empty($domain) ? Tinebase_Config::getGlobalConfigFileNamePath(): Tinebase_Config::getDomainConfigFileNamePath($domain));

            $config = require ($file);

            Tinebase_Config::getInstance()->writeConfigToFile(array_merge($config, array($name => $value)), true);

            Tinebase_Config::getInstance()->resetConfigFile($domain);

            echo "\nValue of $name now is " . print_r(Tinebase_Config::getInstance()->get($name), true) .  " \n\n";
            break;
        default:
            if ($command !== ''){
                echo "Option \"" . $command . "\" not valid\n";
            }
            showConfigHelp($argv[0]);
            exit;
    }
} catch (Exception $e) {
    echo "\n" . $e->getMessage() ."\n\n";
}

/**
 * Show options for script
 */
function showConfigHelp($scriptName)
{
    echo "Expresso Config Reader and Writer\n";
    echo "\n";
    echo "To get configs usage:\n";
    echo "  ".$scriptName." get [key] [domain]\n";
    echo "  ".$scriptName." get [JSON] [domain]\n";
    echo "\n";
    echo "To get only the value of a configs usage:\n";
    echo "  ".$scriptName." getValue [key] [domain]\n";
    echo "  ".$scriptName." getValue [JSON] [domain]\n";
    echo "\n";
    echo "To set configs usage:\n";
    echo "  ".$scriptName." set [key] [value] [domain]\n";
    echo "\n";
    echo " - \"value\" can be a single value or a JSON object. JSON Object Example: '{\"key\": \"value\"}'\n";
    echo " - \"domain\" when not set, global config is the default target\n\n";
}